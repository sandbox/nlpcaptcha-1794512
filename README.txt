
NLPCaptcha for Drupal
====================

The NLPCaptcha module uses the NLPCaptcha web service to
improve the CAPTCHA system and protect email addresses. For
more information on what NLPCaptcha is, please visit:
    http://nlpcaptcha.in


INSTALLATION
------------

1. Extract the NLPCaptcha module to your local favourite
   modules directory (sites/all/modules).


CONFIGURATION
-------------
   
1. Enable NLPCaptcha and CAPTCHA modules in:
       admin/build/modules

2. You'll now find a NLPCaptcha tab in the CAPTCHA
   administration page available at:
       admin/config/people/captcha/nlpcaptcha

3. Register for NLPCaptcha API keys at:
       http://nlpcaptcha.in

4. Input the keys into the NLPCaptcha settings. The rest of
   the settings should be fine as their defaults.

5. Visit the Captcha administration page and set where you
   want the NLPCaptcha form to be presented:
       admin/user/captcha



MULTI-DOMAIN SUPPORT
--------------------

Since NLPCaptcha uses API keys that are unique to each
domain, if you're using a multi-domain system using the
same database, the NLPCaptcha module won't work when
querying the NLPCaptcha web service.  If you put the
following into your sites/mysite/settings.php file for
each domain, it will override the API key values and make
it so multi-domain systems are capable.

  $conf = array(
    'nlpcaptcha_publisher_key' =>  'my other public key',
    'nlpcaptcha_private_key' =>  'my other private key',
    'nlpcaptcha_validate_key' =>  'my other validate key', 
  );

THANK YOU
---------

 * Thank you goes to the NLPCaptcha team for all their
   help, support and their amazing Captcha solution
       http://www.nlpcaptcha.in