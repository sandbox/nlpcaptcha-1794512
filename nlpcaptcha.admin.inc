<?php

/**
 * @file
 * Provides the NLPCaptcha administration settings.
 */

/**
 * Form callback; administrative settings for NLPCaptcha.
 */
function nlpcaptcha_admin_settings() {
  // Load the nlpcaptcha library.
  _nlpcaptcha_load_library();

  $form = array();
  $form['nlpcaptcha_publisher_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher Key'),
    '#default_value' => variable_get('nlpcaptcha_publisher_key', ''),
    '#maxlength' => 40,
    '#description' => t('The publisher key given to you when you <a href="http://nlpcaptcha.in" target="_blank">registered at NLPCaptcha</a>.'),
    '#required' => TRUE,
  );
  $form['nlpcaptcha_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#default_value' => variable_get('nlpcaptcha_private_key', ''),
    '#maxlength' => 40,
    '#description' => t('The private key given to you when you <a href="http://nlpcaptcha.in" target="_blank">registered at NLPCaptcha</a>.'),
    '#required' => TRUE,
  );
  $form['nlpcaptcha_validate_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Validate Key'),
    '#default_value' => variable_get('nlpcaptcha_validate_key', ''),
    '#maxlength' => 40,
    '#description' => t('The validate key given to you when you <a href="http://nlpcaptcha.in" target="_blank">registered at NLPCaptcha</a>.'),
    '#required' => TRUE,
  );
  
 

  return system_settings_form($form);
}

/**
 * Validation function for nlpcaptcha_admin_settings().
 *
 * @see nlpcaptcha_admin_settings()
 */
function nlpcaptcha_admin_settings_validate($form, &$form_state) {
  $tabindex = $form_state['values']['nlpcaptcha_tabindex'];
  if (!empty($tabindex) && !is_numeric($tabindex)) {
    form_set_error('nlpcaptcha_tabindex', t('The Tab Index must be an integer.'));
  }
}
